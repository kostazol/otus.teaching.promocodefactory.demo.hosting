﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.HttpSys;

namespace Otus.Teaching.PromoCodeFactory.WebHost.HostingExtensions
{
    public static class HostingExtensions
    {
        public static void UseHttpSysDefaults(this IWebHostBuilder webBuilder)
        {
            webBuilder.UseHttpSys(options =>
            {
                //Указывает, разрешены ли синхронные операции ввода-вывода
                options.AllowSynchronousIO = false;
                //Указывает, разрешены ли синхронные операции ввода-вывода
                options.Authentication.Schemes = AuthenticationSchemes.None;
                //Разрешает анонимные запросы
                options.Authentication.AllowAnonymous = true;
                options.MaxConnections = null;
                //Размер запроса, null - не ограничен
                options.MaxRequestBodySize = 30000000;
                options.UrlPrefixes.Add("http://localhost:5005");
            });
        }
    }
}